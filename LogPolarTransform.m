function out = LogPolarTransform(img, rmin, scale_factor, nr, nw)
%LOGPOLARTRANSFORM 此处显示有关此函数的摘要
%   此处显示详细说明
    [h, w, d] = size(img);
    if d==1
        out = zeros([nr,nw]);
    else
        out = zeros([nr,nw,d]);
    end
    ss = 1:nr;
    rho = rmin * scale_factor .^ ss;
    rho = rho';
    theta = linspace(2*pi, 0, nw+1);
    theta(1) = [];
    xx = rho*cos(theta)+ w/2;
    yy = rho*sin(theta)+ h/2;
    if d==1
        out = interp2(img,xx,yy,'linear',0);
    else
        for l=1:d
            out(:,:,l) = interp2(img(:,:,l),xx,yy,'linear',0);
        end
    end
end


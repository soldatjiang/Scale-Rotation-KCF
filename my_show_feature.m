function my_show_feature(feature)
    nChannels = size(feature, 3);
    nCols = floor(sqrt(nChannels));
    nRows = ceil(nChannels / nCols);
    [h,w,~] = size(feature);
    blank = zeros([h, w]);
    for p=1:nCols*nRows
        subplot(nRows, nCols, p)
        if p<=nChannels
            imshow(feature(:,:,p),[]);
        else
            imshow(blank,[]);
        end
    end
end


function out = LogTransform(img, nx, ny)
    [h, w, d] = size(img);
    if d==1
        out = zeros([ny,nx]);
    else
        out = zeros([ny,nx,d]);
    end
    xx = 1:nx;
    xx = 1.02.^(xx);
    %xx = xx - floor(nx/2);
    %yy = 1:ny;
    yy = 1:ny;
    yy = 1.02.^(yy);
    yy = yy';
    %yy = yy - floor(ny/2);
    if d==1
        out = interp2(img,xx,yy,'linear',0);
    else
        for l=1:d
            out(:,:,l) = interp2(img(:,:,l),xx,yy,'linear',0);
        end
    end
end


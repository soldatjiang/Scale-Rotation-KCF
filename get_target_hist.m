function out = get_target_hist( im, pos, target_sz)
    out = zeros(1,4096);
    xs = floor(pos(2)) + (1:target_sz(2)) - floor(target_sz(2)/2);
    ys = floor(pos(1)) + (1:target_sz(1)) - floor(target_sz(1)/2);
    
    %check for out-of-bounds coordinates, and set them to the values at
    %the borders
    xs(xs < 1) = 1;
    ys(ys < 1) = 1;
    xs(xs > size(im,2)) = size(im,2);
    ys(ys > size(im,1)) = size(im,1);
    
    target_patch = im(ys, xs, :);
    
    target_height = size(target_patch,1);
    target_width = size(target_patch,2);
    
    w_sum = 0;
    
    center = [pos(1)-min(ys(:)),pos(2)-min(xs(:))];
    
    for y=1:target_height
        for x=1:target_width
            % weight at current pixel
            diff = ([y,x]-center)./[target_height,target_width];
            w = 1-diff*diff';
            rv = floor(double(target_patch(y,x,1))/16);
            gv = floor(double(target_patch(y,x,2))/16);
            bv = floor(double(target_patch(y,x,3))/16);
            %[im(y,x,1), im(y,x,2), im(y,x,3)]
            %[rv,gv,bv]
            cindex = rv*256 + gv*16 + bv+1;
            if w>0
                out(1,cindex) = out(1, cindex)+w;
                w_sum = w_sum+w;
            end
        end
    end
    
    %normalization to ensure the sum of the histogram to be 1
    out = out./w_sum;
end


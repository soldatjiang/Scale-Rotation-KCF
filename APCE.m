function result = APCE(response)
    max_res = max(response(:));
    min_res = min(response(:));
    square_diff = (response - min_res).^2;
    result = (max_res - min_res).^2 / mean(square_diff(:));
end


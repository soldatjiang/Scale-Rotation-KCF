function out = get_feature_map(im_patch, features, tg_hist, cxt_hist)

% out = get_feature_map(im_patch, features, w2c)
%
% Extracts the given features from the image patch. w2c is the
% Color Names matrix, if used.

% the names of the features that can be used
valid_features = {'gray', 'cr'};

% the dimension of the valid features
feature_levels = [1 1]';

num_valid_features = length(valid_features);
used_features = false(num_valid_features, 1);

% get the used features
for i = 1:num_valid_features
    used_features(i) = any(strcmpi(valid_features{i}, features));
end

% total number of used feature levels
num_feature_levels = sum(feature_levels .* used_features);

level = 0;

% If grayscale image
if size(im_patch, 3) == 1
    % Features that are available for grayscale sequances
    out = zeros(size(im_patch, 1), size(im_patch, 2), num_feature_levels);
    % Grayscale values (image intensity)
    if used_features(1)
        out = double(im_patch)/255 - 0.5;
        level = level + feature_levels(1);
    end
    
    % Color ratios
    if used_features(2)
        cxt2_hist = cxt_hist{1};
        cxt3_hist = cxt_hist{2};
        cxt4_hist = cxt_hist{3};
        out(:,:,level+1) = get_colour_map(im_patch,tg_hist,cxt2_hist);
        out(:,:,level+2) = get_colour_map(im_patch,tg_hist,cxt3_hist);
        out(:,:,level+3) = get_colour_map(im_patch,tg_hist,cxt4_hist);
    end
else
    % Features that are available for color sequances
    
    % allocate space (for speed)
    out = zeros(size(im_patch, 1), size(im_patch, 2), num_feature_levels);
    
    % Grayscale values (image intensity)
    if used_features(1)
        out(:,:,level+1) = double(rgb2gray(im_patch))/255 - 0.5;
        level = level + feature_levels(1);
    end
    
    % Color ratios
    if used_features(2)
        cxt2_hist = cxt_hist{1};
        cxt3_hist = cxt_hist{2};
        cxt4_hist = cxt_hist{3};
        out(:,:,level+1) = get_colour_map(im_patch,tg_hist,cxt2_hist);
        out(:,:,level+2) = get_colour_map(im_patch,tg_hist,cxt3_hist);
        out(:,:,level+3) = get_colour_map(im_patch,tg_hist,cxt4_hist);
    end
end
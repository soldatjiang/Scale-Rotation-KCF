function colour_map = get_colour_map(patch, tg_hist, cxt_hist)
    [h, w, d] = size(patch);
    bin_width = 256/16;
    patch_array = reshape(double(patch), w*h, d);
    bin_indices = floor(patch_array/bin_width)+1;
    if d==1
        hist_indices = bin_indices;
    else
        hist_indices = sub2ind(size(tg_hist),bin_indices(:,1),bin_indices(:,2),bin_indices(:,3));
    end
     cxt_hist(find(cxt_hist==0))=1;
     ratio_table = sqrt(tg_hist./cxt_hist);
     norm = max(ratio_table(:));
     colour_map = reshape(ratio_table(hist_indices), h, w) / norm;
%     tg_map = tg_hist(hist_indices);
%     cxt_map = cxt_hist(hist_indices);
%     colour_map = reshape(sqrt(tg_map./cxt_map), h, w);
%     colour_map = colour_map / max(colour_map(:));
end


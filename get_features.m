function x = get_features(im, features, hog_params, cr_params, cos_window)
    if features.hog,
		%HOG features, from Piotr's Toolbox
        cell_size = hog_params.cell_size;
        bins = hog_params.bins;
		x = double(fhog(single(im) / 255, cell_size, bins));
		x(:,:,end) = [];  %resz = repmat(sqrt(prod(target_sz * param.search_area_scale)), 1, 2); % square area, ignores the target aspect ratiomove all-zeros channel ("truncation feature")
%         sz = size(x);
% 		im_patch = mresize(im, [sz(1) sz(2)]);
%         x = cat(3,x,im_patch);
    end
    
    if features.hogcr
		%HOG features, from Piotr's Toolbox
        cell_size = hog_params.cell_size;
        bins = hog_params.bins;
		x = double(fhog(single(im) / 255, cell_size, bins));
		x(:,:,end) = [];  %remove all-zeros channel ("truncation feature")
		sz = size(x);
        target_hist = cr_params.target_hist;
        context_hist = cr_params.context_hist;
		im_patch = mresize(im, [sz(1) sz(2)]);
		out_gray = get_feature_map(im_patch, 'gray', target_hist, context_hist);
		out_cr = get_feature_map(im_patch, 'cr', target_hist, context_hist);
% 		out_pca = reshape(temp_pca, [prod(sz), size(temp_pca, 3)]);
		x = cat(3,x,out_gray);
		x = cat(3,x,out_cr);
    end

	
	if features.gray,
		%gray-level (scalar feature)
		x = double(im) / 255;
		x = x - mean(x(:));
	end
	
	%process with cosine window if needed
	if nargin > 4 
        x = bsxfun(@times, x, cos_window);
	end
end
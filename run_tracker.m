% run_tracker.m

close all;
% clear all;

%choose the path to the videos (you'll be able to choose one with the GUI)
base_path = 'D:\JiangShan\data_seq\otb100\Benchmark\';

%parameters according to the paper
params.padding = 1.5;         			   % extra area surrounding the target
params.output_sigma_factor = 0.1;		   % spatial bandwidth (proportional to target)
params.sigma = 0.5;         			   % gaussian kernel bandwidth
params.lambda = 1e-4;					   % regularization (denoted "lambda" in the paper)
params.learning_rate = 0.02;			   % learning rate for appearance model update scheme (denoted "gamma" in the paper)
params.features.gray = false;
params.features.hog = false;
params.features.hogcr = true;
params.scale_rot_features.hog = true;
params.scale_rot_features.gray = false;
params.scale_rot_features.hogcr = false;
params.hog_params.cell_size = 4;
params.hog_params.bins = 9;

params.visualization = 1;

%ask the user for the video
video_path = choose_video(base_path);
if isempty(video_path), return, end  %user cancelled
[img_files, pos, target_sz, ground_truth, video_path] = ...
	load_video_info(video_path);

params.init_pos = floor(pos) + floor(target_sz/2);
params.wsize = floor(target_sz);
params.img_files = img_files;
params.video_path = video_path;

[positions, fps] = tracker(params);

% calculate precisions
[distance_precision, PASCAL_precision, average_center_location_error] = ...
    compute_performance_measures(positions, ground_truth);

fprintf('Center Location Error: %.3g pixels\nDistance Precision: %.3g %%\nOverlap Precision: %.3g %%\nSpeed: %.3g fps\n', ...
    average_center_location_error, 100*distance_precision, 100*PASCAL_precision, fps);

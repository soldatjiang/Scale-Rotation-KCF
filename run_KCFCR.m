function results=run_KCFCR(seq, res_path, bSaveImage, parameters)

%choose the path to the videos (you'll be able to choose one with the GUI)
%base_path = 'sequences/';

params.padding = 1.5;         			   % extra area surrounding the target
params.output_sigma_factor = 0.1;		   % spatial bandwidth (proportional to target)
params.sigma = 0.5;         			   % gaussian kernel bandwidth
params.lambda = 1e-4;					   % regularization (denoted "lambda" in the paper)
params.learning_rate = 0.02;			   % learning rate for appearance model update scheme (denoted "gamma" in the paper)
params.features.gray = false;
params.features.hog = false;
params.features.hogcr = true;
params.scale_rot_features.hog = true;
params.scale_rot_features.gray = false;
params.scale_rot_features.hogcr = false;
params.hog_params.cell_size = 4;
params.hog_params.bins = 9;

params.visualization = 0;

% %ask the user for the video
% video_path = choose_video(base_path);
% if isempty(video_path), return, end  %user cancelled
% [img_files, pos, target_sz, ground_truth, video_path] = ...
% 	load_video_info(video_path);

target_sz = seq.init_rect(1,[4,3]);
pos = seq.init_rect(1,[2,1]) + floor(target_sz/2);
img_files = seq.s_frames;
video_path = [];

%params.init_pos = floor(pos) + floor(target_sz/2);
params.init_pos = pos;
params.wsize = floor(target_sz);
params.img_files = img_files;
params.video_path = video_path;

[positions, fps] = tracker(params);

%return results to benchmark, in a workspace variable
rects = [positions(:,2) - target_sz(2)/2, positions(:,1) - target_sz(1)/2];
rects(:,3) = target_sz(2);
rects(:,4) = target_sz(1);
disp(['fps: ' num2str(fps)])
results.type = 'rect';
results.res = rects;
results.fps = fps;